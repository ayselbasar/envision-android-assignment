package com.example.envisionapp.data.adapter

import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.envisionapp.R
import com.example.envisionapp.data.db.entity.LibraryData
import com.example.envisionapp.databinding.ItemViewLibraryBinding

class LibraryAdapter(
    private var values: List<LibraryData>
) : RecyclerView.Adapter<LibraryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemViewLibraryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_view_library,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        values.let {
            holder.binding.libraryData = it[position]
            holder.binding.executePendingBindings()
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: ItemViewLibraryBinding) :
        RecyclerView.ViewHolder(binding.root)
}
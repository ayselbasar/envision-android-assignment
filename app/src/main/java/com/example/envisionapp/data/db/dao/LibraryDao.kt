package com.example.envisionapp.data.db.dao

import androidx.room.*
import com.example.envisionapp.data.db.entity.LibraryData

@Dao
interface LibraryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertInfo(library: LibraryData): Long

    @Query("SELECT * FROM library_data_table")
    fun getAllInfo(): List<LibraryData>
}
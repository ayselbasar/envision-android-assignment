package com.example.envisionapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "library_data_table",
    indices = [Index(
        value = ["library_id"],
        unique = true
    )]
)
data class LibraryData(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "library_id")
    var id: Int,

    @ColumnInfo(name = "library_date_time")
    var dateTime: String,

    @ColumnInfo(name = "library_info")
    var info: String,
)
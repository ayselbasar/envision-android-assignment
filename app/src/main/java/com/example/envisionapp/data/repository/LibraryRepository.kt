package com.example.envisionapp.data.repository

import android.app.Application
import com.example.envisionapp.data.db.MainDatabase
import com.example.envisionapp.data.db.dao.LibraryDao
import com.example.envisionapp.data.db.entity.LibraryData
import java.util.concurrent.Callable
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class LibraryRepository(
    application: Application
) {
    private var database: MainDatabase =
        MainDatabase.getDatabase(
            application
        )

    private var libraryDao: LibraryDao? = database.libraryDao()

    private val executor: Executor = Executors.newFixedThreadPool(2)

    fun insert(libraryData: LibraryData): Long {
        val callable = Callable {
            libraryDao!!.insertInfo(libraryData)
        }
        val future = Executors.newSingleThreadExecutor().submit(callable)
        return future.get().toLong()
    }

    fun getInfoList(): List<LibraryData> {
        val callable = Callable {
            libraryDao!!.getAllInfo()
        }
        val future = Executors.newSingleThreadExecutor().submit(callable)
        return future.get()
    }
}
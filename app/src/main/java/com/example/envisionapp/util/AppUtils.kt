package com.example.envisionapp.util

import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.example.envisionapp.R
import com.example.envisionapp.util.AppUtil.Companion.statusMessage
import com.google.android.material.snackbar.Snackbar

class AppUtil {
    companion object {
        val statusMessage = MutableLiveData<Event<String>>()
    }
}

fun showError(tag: String, message: String) {
    Log.e(tag, message)
}

fun showMessage(message: String) {
    statusMessage.value = Event(message)
}

fun showSnackBar(view: View, message: String, action: String? = "", callback: (Boolean) -> Unit) {

    val snackBar = Snackbar
        .make(view, message, Snackbar.LENGTH_LONG)

    if (!action.isNullOrEmpty()) {
        snackBar.setAction(action) {
            callback.invoke(true)
        }
    }

    val snackBarView = snackBar.view

    snackBarView.setBackgroundColor(ContextCompat.getColor(view.context, R.color.white))
    snackBar.setActionTextColor(ContextCompat.getColor(view.context, R.color.color_active))

    val textView =
        snackBarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
    textView.setTextColor(ContextCompat.getColor(view.context, R.color.color_snackbar_text))
    snackBar.show()

    callback.invoke(false)
}


//for scrollable TextView inside NestedScrollView.
fun enableScroll(view: View) {
    if (view is TextView) {
        view.movementMethod = ScrollingMovementMethod()
    }
}
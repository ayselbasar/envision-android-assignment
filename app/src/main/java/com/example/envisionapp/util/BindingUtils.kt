package com.example.envisionapp.util

import android.net.Uri
import android.widget.Button
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.envisionapp.R

@BindingAdapter("imageViewUri")
fun setImage(view: ImageView, uri: Uri?) {
    uri?.let {
        Glide.with(view.context)
            .load(uri)
            .into(view)
    }
}

@BindingAdapter("enable")
fun setBackground(view: Button, isEnable: Boolean) {
    if (isEnable) {
        view.background = ContextCompat.getDrawable(view.context, R.drawable.button_rounded)
    } else {
        view.background = ContextCompat.getDrawable(view.context, R.drawable.button_rounded_passive)
    }
}





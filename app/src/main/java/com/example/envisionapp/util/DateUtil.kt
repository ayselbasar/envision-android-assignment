package com.example.envisionapp.util

import java.text.SimpleDateFormat
import java.util.*

const val patternDateTime = "dd/MM/yy HH:mm"

fun getCurrentDate(): Date {
    return Date()
}

fun formatDate(date: Date, pattern: String): String {
    val simpleDateFormat = SimpleDateFormat(pattern, Locale.getDefault())
    return simpleDateFormat.format(date)
}
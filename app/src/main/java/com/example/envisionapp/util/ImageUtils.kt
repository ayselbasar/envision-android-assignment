package com.example.envisionapp.util

import android.app.Activity
import android.content.ContentValues.TAG
import android.net.Uri
import android.os.Environment
import android.util.Log
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.example.envisionapp.R
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

lateinit var imageCapture: ImageCapture
lateinit var cameraExecutor: ExecutorService

fun FragmentActivity.startCamera(viewFinder: PreviewView) {

    val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
    cameraProviderFuture.addListener(Runnable {
        // Used to bind the lifecycle of cameras to the lifecycle owner
        val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

        // Preview
        val preview = Preview.Builder()
            .build()
            .also {
                it.setSurfaceProvider(viewFinder.createSurfaceProvider())
            }

        imageCapture = ImageCapture.Builder().build()

        // Select back camera as a default
        val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

        try {
            // Unbind use cases before rebinding
            cameraProvider.unbindAll()

            // Bind use cases to camera
            cameraProvider.bindToLifecycle(
                this, cameraSelector, preview, imageCapture
            )
        } catch (exc: Exception) {
            showError(TAG, getString(R.string.message_failed_start_camera))
        }
    }, ContextCompat.getMainExecutor(this))
}

fun Activity.capturePhoto(callback: (Uri) -> Unit) {

    // Get a stable reference of the modifiable image capture use case
    val imageCapture = imageCapture

    // Create output file to hold the image
    val fileUniqueName = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
    val imageFileName = "$fileUniqueName.jpg"
    val storageFile = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
    val photoFile = File(storageFile, imageFileName)

    // Create output options object which contains file + metadata
    val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

    // Set up image capture listener, which is triggered after photo has been taken
    imageCapture.takePicture(
        outputOptions,
        ContextCompat.getMainExecutor(this),
        object : ImageCapture.OnImageSavedCallback {
            override fun onError(exception: ImageCaptureException) {
                showError(
                    TAG,
                    "${getString(R.string.message_failed_capture_photo)}: ${exception.message}"
                )
            }

            override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                val imageUri = Uri.fromFile(photoFile)
                val message = "${getString(R.string.message_succeeded_capture_photo)}: $imageUri"
                Log.d(TAG, message)
                callback.invoke(imageUri)
            }
        })
}

fun FragmentActivity.openCamera(callback: (Boolean) -> Unit) {
    if (PermissionUtils.checkPermissions(this)) {
        callback.invoke(true)
    } else {
        PermissionUtils.requestMultiplePermissions(this)
        callback.invoke(false)
    }
    cameraExecutor = Executors.newSingleThreadExecutor()
}

fun FragmentActivity.detectTextFromImage(
    imageUri: Uri,
    callback: (String) -> Unit
) {
    val image = FirebaseVisionImage.fromFilePath(
        applicationContext,
        imageUri
    )

    // On-device API
    val detector = FirebaseVision.getInstance().onDeviceTextRecognizer

    detector.processImage(image)
        .addOnSuccessListener { result ->
            // Task completed successfully..Text recognition results
            val resultText = processTextRecognitionResult(result)
            callback.invoke(resultText)
        }.addOnFailureListener {
            // Task failed with an exception
            showError(TAG, it.message.toString())
            showMessage(getString(R.string.message_failed_recognize_text))
        }
}

private fun FragmentActivity.processTextRecognitionResult(
    texts: FirebaseVisionText
): String {
    if (texts.textBlocks.size == 0) {
        showMessage(getString(R.string.message_no_text_found))
    }
    return texts.text
}





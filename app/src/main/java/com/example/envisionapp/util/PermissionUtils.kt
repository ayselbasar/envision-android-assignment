package com.example.envisionapp.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat

class PermissionUtils {

    companion object {
        const val requestCode = 100

        private val permissions: Array<String> =
            arrayOf(
                "${Manifest.permission.CAMERA}",
                "${Manifest.permission.WRITE_EXTERNAL_STORAGE}",
                "${Manifest.permission.READ_EXTERNAL_STORAGE}"
            )

        @JvmStatic
        fun checkPermissions(context: Context): Boolean {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(
                        context,
                        permission
                    ) != PackageManager.PERMISSION_GRANTED
                )
                    return false
            }
            return true
        }

        @JvmStatic
        fun requestMultiplePermissions(activity: Activity) {
            val remainingPermissions = mutableListOf<String>()

            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(
                        activity,
                        permission
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    remainingPermissions.add(permission)
                }
            }

            ActivityCompat.requestPermissions(
                activity,
                remainingPermissions.toTypedArray(),
                requestCode
            )
        }
    }
}


fun Activity.requestPermissionsResult(
    requestCode: Int,
    permissions: Array<out String>,
    grantResults: IntArray
): Boolean {

    if (requestCode == PermissionUtils.requestCode) {

        for (i in 0 until grantResults.count()) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        permissions[i]
                    )
                ) {
                }
                return false
            }
        }
    }
    return true
}
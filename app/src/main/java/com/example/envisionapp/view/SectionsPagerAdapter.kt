package com.example.envisionapp.view

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.envisionapp.R
import com.example.envisionapp.view.fragment.CaptureFragment
import com.example.envisionapp.view.fragment.LibraryFragment

private val TAB_TITLES = arrayOf(
    R.string.tab_capture,
    R.string.tab_library
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            CaptureFragment.newInstance(position)
        } else {
            LibraryFragment.newInstance(position)
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return TAB_TITLES.count()
    }
}
package com.example.envisionapp.view.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.example.envisionapp.R
import com.example.envisionapp.databinding.ActivityMainBinding
import com.example.envisionapp.util.openCamera
import com.example.envisionapp.util.requestPermissionsResult
import com.example.envisionapp.util.showSnackBar
import com.example.envisionapp.util.startCamera
import com.example.envisionapp.view.SectionsPagerAdapter
import com.example.envisionapp.viewModel.SharedViewModel
import kotlinx.android.synthetic.main.content_capture.view.*
import kotlinx.android.synthetic.main.fragment_capture.*

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private val viewModel: SharedViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Inflate view and obtain an instance of the binding class.
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        // Specify the current activity as the lifecycle owner.
        binding.lifecycleOwner = this

        binding.sharedViewModel = viewModel

        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        binding.viewPager.adapter = sectionsPagerAdapter

        binding.tabLayout.setupWithViewPager(binding.viewPager)

        binding.viewPager.currentItem = 1

        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        viewModel.imageUri.value = null
                        viewModel.contentVisibility.value = 0
                        viewModel.isEnableCaptureButton.value = false
                        openCamera { result ->
                            if (result) {
                                viewModel.isEnableCaptureButton.value = true
                            }
                        }
                    }
                    else -> {
                        viewModel.getLibrary()
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })

        viewModel.isEnableCaptureButton.observe(this, { result ->
            if (result)
                startCamera(layoutCapture.viewFinder)
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (this.requestPermissionsResult(
                requestCode,
                permissions,
                grantResults
            )
        ) {
            viewModel.isEnableCaptureButton.value = true

            showSnackBar(
                binding.viewPager,
                getString(R.string.message_permission_granted)
            ) {}
        }
    }

}

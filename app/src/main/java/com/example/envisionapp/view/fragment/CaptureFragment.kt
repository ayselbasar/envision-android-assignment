package com.example.envisionapp.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.envisionapp.databinding.FragmentCaptureBinding
import com.example.envisionapp.util.*
import com.example.envisionapp.viewModel.SharedViewModel

class CaptureFragment : Fragment() {

    lateinit var binding: FragmentCaptureBinding
    private val viewModel: SharedViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            FragmentCaptureBinding.inflate(
                inflater,
                container,
                false
            ).apply {
                lifecycleOwner = viewLifecycleOwner
                sharedViewModel = viewModel
            }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.layoutCapture.btnCapture.setOnClickListener {
            onClickCapture()
        }

        viewModel.imageUri.observe(viewLifecycleOwner, { uri ->
            viewModel.contentVisibility.value = 1
            uri?.let {
                requireActivity().detectTextFromImage(uri) { text ->
                    viewModel.recognizedText.value = text
                }
            }
        })

        viewModel.recognizedText.observe(viewLifecycleOwner, { text ->
            viewModel.contentVisibility.value = 2
            viewModel.isEnableSaveButton.value = true
            binding.layoutText.txtVwRecognizedText.text = text
        })

        viewModel.contentVisibility.value = 0

        //for scrollable textView..
        enableScroll(binding.layoutText.txtVwRecognizedText)
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): CaptureFragment {
            return CaptureFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }

    private fun onClickCapture() {
        if (viewModel.isEnableCaptureButton.value == false)
            return

        requireActivity().capturePhoto { uri ->
            viewModel.imageUri.value = uri
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }
}
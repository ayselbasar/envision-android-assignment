package com.example.envisionapp.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import com.example.envisionapp.data.adapter.LibraryAdapter
import com.example.envisionapp.databinding.FragmentLibraryBinding
import com.example.envisionapp.viewModel.SharedViewModel

class LibraryFragment : Fragment() {

    lateinit var binding: FragmentLibraryBinding
    private val viewModel: SharedViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            FragmentLibraryBinding.inflate(
                inflater,
                container,
                false
            ).apply {
                lifecycleOwner = viewLifecycleOwner
                sharedViewModel = viewModel
            }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Set the library vertical recyclerView
        with(binding.recyclerViewLibraryList) {
            layoutManager =
                androidx.recyclerview.widget.GridLayoutManager(
                    context,
                    1,
                    RecyclerView.VERTICAL,
                    false
                )
        }

        viewModel.getLibrary()

        viewModel.libraryList.observe(viewLifecycleOwner, { list ->
            //show all library info inside recyclerview.
            viewModel.libraryAdapter = LibraryAdapter(list.toMutableList())
            binding.recyclerViewLibraryList.adapter = viewModel.libraryAdapter
        })
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): LibraryFragment {
            return LibraryFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}
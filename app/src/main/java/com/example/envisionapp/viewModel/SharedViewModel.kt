package com.example.envisionapp.viewModel

import android.app.Application
import android.net.Uri
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.envisionapp.R
import com.example.envisionapp.data.adapter.LibraryAdapter
import com.example.envisionapp.data.db.entity.LibraryData
import com.example.envisionapp.data.repository.LibraryRepository
import com.example.envisionapp.util.formatDate
import com.example.envisionapp.util.getCurrentDate
import com.example.envisionapp.util.patternDateTime
import com.example.envisionapp.util.showSnackBar

class SharedViewModel(
    application: Application
) : AndroidViewModel(application), Observable {

    private var libraryRepository: LibraryRepository = LibraryRepository(application)
    val imageUri = MutableLiveData<Uri?>()
    val recognizedText = MutableLiveData<String>()
    val contentVisibility = MutableLiveData<Int>()

    @Bindable
    val isEnableSaveButton = MutableLiveData<Boolean>()

    @Bindable
    val isEnableCaptureButton = MutableLiveData<Boolean>()

    @Bindable
    val libraryList = MutableLiveData<List<LibraryData>>()

    var libraryAdapter: LibraryAdapter? = null

    fun onClickSaveText(view: View) {

        if (isEnableSaveButton.value == false) {
            return
        }

        recognizedText.value?.let { text ->
            //insert to room db.
            insertTextToLibrary(text) { result ->

                if (result) {
                    view.background =
                        ContextCompat.getDrawable(view.context, R.drawable.button_rounded_passive)
                    isEnableSaveButton.value = false
                    showSnackBar(
                        view,
                        view.context.getString(R.string.message_text_saved_text),
                        view.context.getString(R.string.message_button_saved_text)
                    ) {
                    }
                } else {
                    showSnackBar(
                        view,
                        view.context.getString(R.string.message_failed_save_text)
                    ) {

                    }
                }
            }
        } ?: kotlin.run {
            showSnackBar(
                view,
                view.context.getString(R.string.message_failed_save_text)
            ) {

            }
        }
    }


    fun getLibrary() {
        libraryList.value = libraryRepository.getInfoList()
    }

    private fun insertTextToLibrary(text: String, callback: (Boolean) -> Unit) {

        //get current date
        val currentDate = getCurrentDate()
        val formattedDate = formatDate(currentDate, patternDateTime)

        val libraryData = LibraryData(
            id = 0,
            dateTime = formattedDate,
            info = text
        )

        val insertedInfo = libraryRepository.insert(libraryData)
        if (insertedInfo > -1) {
            callback.invoke(true)
        } else {
            callback.invoke(false)
        }
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }
}